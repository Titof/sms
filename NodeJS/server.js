var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var link = new Object();

var makeLink = function(id1, id2) {
  link[id1] = id2;
  link[id2] = id1;
}

var delLink = function(id) {
  delete link[link[id]];
  delete link[id];
}

var sendTo = function (id, chan, msg) {
  io.to(id).emit(chan, msg);
}

app.get('/', function(req, res){
  res.sendFile(__dirname +'/index.html');
});

io.on('connection', function(socket){

  // Gestion de connexion du webclient
  socket.on('connection-web', function(){
    console.log("Connection web : "+socket.id);
    sendTo(socket.id, 'connection-web', { "id" : socket.id });
        console.log(Object.keys(io.engine.clients));
  });

  // Gestion de connexion du telephone
  socket.on('connection-phone', function(data){
    console.log("Connection phone : " +socket.id);
    // Envoie au webclient demande de connexion du telephone
    console.log(data);
    sendTo(data.id, 'connection-phone', { "id" : socket.id, "data" : data.data});
  });

  // Gestion du connexion result
  socket.on('connection-result', function(data) {
    console.log("Connection result : "+data.result);
    sendTo(data.id, 'connection-result', data);

    if(data.result) {
      makeLink(socket.id, data.id);
    }
  });

  // Disconnect

  socket.on('disconnect', function(){
      if (link[socket.id] != null) {
        sendTo(link[socket.id], 'disconnect-socket', { "id" : socket.id });
      }
  });

  // Data
  socket.on('data', function(data) {

    if (Object.keys(io.engine.clients).indexOf(data.id) == -1) {
      console.log(" Fail "+socket.id);
      sendTo(socket.id, 'fail', data);
      return;
    }

    console.log(" Data "+data)
    sendTo(data.id, 'data', {"id":socket.id, "data":data.data});
  });

  // Ping
  socket.on('ping', function(data) {
    sendTo(data.id, 'ping', {"id":socket.id});
  });
  
});

http.listen(3001, function(){
  console.log('listening on *:3001');
});