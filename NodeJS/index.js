var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var clients = {};
var whois = {};

var gClient = function(socket) {
	return {
		//"code" :  Math.floor((Math.random() * 100) + 1),
		"code" : 42,
		"socket" : socket.id,
		"phone" : null
	};
}


var deleteClient = function(id) {
	if (whois[id] != null) {
		if (whois[id] == "web") {
			io.to(clients[id][client]).emit("web-disconnect", "disconnect");
			delete clients[id];
			delete whois[id];
		} else if(whois[id] == "phone"){
			id_web = searchClientByPhone(id);
			io.to(clients[id_web]["phone"]).emit("phone-disconnect", "disconnect");
			clients[id_web]["phone"] = null;
			delete whois[id];
		}
	}

}

var searchClientByCode = function(code) {
	for (var key in clients) {
  		if (clients[key]["code"] == code) {
  			return key;
  		}
	}
	return null;
}

var searchClientByPhone = function(id) {
	for (var key in clients) {
  		if (clients[key]["phone"] == id) {
  			return key;
  		}
	}
	return null;
}

app.get('/', function(req, res){
  res.sendFile(__dirname +'/index.html');
});

io.on('connection', function(socket){

  socket.on('chat message', function(msg){
  	console.log("msg");
  	  io.emit('chat message', msg);
  });

  socket.on('webclient-connect', function(msg) {
  	clients[socket.id] = gClient(socket);
  	whois[socket.id] = "web";
  	io.emit('webclient-connect',  clients[socket.id].code);

  });

  socket.on('webclient-general', function(msg) {
  	io.emit('chat message', msg);
  });

  socket.on('phone-connect', function(code) {
  	console.log('phone-connect');
  	id = searchClientByCode(code);
  	console.log(' id : ' + id);
  	if (id) {
  		io.emit('phone-connect', 'ok');
  		io.to(id).emit("chat message", "un telephone s'est connecté");
  		clients[id]["phone"] = socket.id;
  		whois[socket.id] = "phone";
  	}
  });

  socket.on('disconnect', function () {
  	deleteClient(socket.id);
  	console.log("disconnect : " + socket.id);
  });

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});