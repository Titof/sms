package fr.kevinstricks.smsqr;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.provider.Telephony.Sms.Conversations;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class MainActivity extends AppCompatActivity {

    private CaptureManager capture;
    private CompoundBarcodeView barcodeScannerView;
    private SmsManager SMSManager;
    private Socket socket;
    private String webid;
    private String passphrase;

    private Key key;
    private String iv;
    private String salt;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SMSManager = SmsManager.getDefault();

        try {
            socket = IO.socket("http://sms.74.re");
        } catch (URISyntaxException e) {}
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**************************************
    *   Get a JSONArray of conversations  *
    ***************************************/
    public JSONArray getConversations(){
        JSONArray conversations = new JSONArray();

        ContentResolver contentResolver = getContentResolver();
        Uri uri = Uri.parse("content://sms/conversations/");
        Cursor query = contentResolver.query(uri, null, null, null, "date desc");

        while (query.moveToNext()){

            String thread_id = query.getString(0);
            String phoneNumber = "";
            String contactName = "";

            uri = Uri.parse("content://sms/");
            Cursor querySms = contentResolver.query(uri,null,"thread_id=" + thread_id ,null,null);
            if (querySms.moveToFirst()) {
                phoneNumber = querySms.getString(2);
            }
            querySms.close();

            uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            Cursor cursor = contentResolver.query(uri, new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
            cursor.close();

            Log.d("->",contactName+" : "+phoneNumber);


            JSONObject conversation = new JSONObject();
            try {
                conversation.put("thread_id",thread_id);
                conversation.put("phoneNumber",phoneNumber);
                conversation.put("contactName",contactName);
                conversation.put("msgCount",query.getString(1));

                //conversation.put("snippet",query.getString(2));
                conversations.put(conversation);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return conversations;
    }

    /***********************************************************
     *   Get a JSONArray of messages in a conversation         *
     *   ----------------------------------------------------- *
     *   thread_id : the thread_id of the conversation         *
     *   nbMessages : the number of messages to get            *
     *   page : 0 -> get messages 1 to nbMessage               *
     *          1 -> get messages nbMessage to nbMessage*2     *
     *          3 -> get messages nbMessage*2 to nbMessage*3   *
     *          etc                                            *
     ***********************************************************/
    public JSONArray getMessages(String thread_id,int nbMessages,int page){
        JSONArray messages = new JSONArray();
        ContentResolver contentResolver = getContentResolver();
        Uri uri = Uri.parse("content://sms/");
        Cursor querySms = contentResolver.query(uri, new String[]{"body"}, "thread_id=" + thread_id, null, null);

        int i =0;
        while(querySms.moveToNext() && i<page*nbMessages+nbMessages) {
            if(i>=page*nbMessages) {
                JSONObject message = new JSONObject();
                try {
                    message.put("message",querySms.getString(0));
                } catch (JSONException e) { e.printStackTrace();  }
                messages.put(message);
            }
            i++;
        }
        querySms.close();
        return  messages;
    }


    /**************
    *   Send SMS  *
    ***************/

    public void sendSMS(String phoneNumber, String msg, String id){

        String sent = "SMS_SENT:"+id;
        String delivery = "SMS_RECEIVED:"+id;

        Intent sentIntent = new Intent(sent);
        sentIntent.putExtra("id",id);
        Intent deliveryIntent = new Intent(delivery);
        deliveryIntent.putExtra("id",id);

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, sentIntent, 0);
        PendingIntent deliveryPI = PendingIntent.getBroadcast(this, 0, deliveryIntent, 0);

        registerReceiver(new BroadcastReceiver(){
            public void onReceive(Context context, Intent intent) {

                if(getResultCode() == Activity.RESULT_OK)
                {
                    Toast.makeText(getBaseContext(), "SMS sent "+intent.getStringExtra("id"),Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getBaseContext(), "SMS could not sent "+intent.getStringExtra("id"),Toast.LENGTH_SHORT).show();
                }
            }
        }, new IntentFilter(sent));

        registerReceiver(new BroadcastReceiver(){
            public void onReceive(Context context, Intent intent) {
                if(getResultCode() == Activity.RESULT_OK)
                {
                    Toast.makeText(getBaseContext(), "SMS received "+intent.getStringExtra("id"),Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getBaseContext(), "SMS not received "+intent.getStringExtra("id"), Toast.LENGTH_SHORT).show();
                }
            }
        }, new IntentFilter(delivery));

        SMSManager.sendTextMessage(phoneNumber, null, msg, sentPI, deliveryPI);
    }

    /*************************
    *  Methods call from UI  *
    **************************/

    public void showMessages(View v){
        JSONArray conversations = getConversations();
        try {
            JSONArray messages = getMessages(conversations.getJSONObject(0).getString("thread_id"),10,0);
            TextView hello = (TextView) findViewById(R.id.hello);
            hello.setText(messages.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showConversation(View v){
        TextView hello = (TextView) findViewById(R.id.hello);
        JSONArray conversations = getConversations();
        hello.setText(conversations.toString());
    }

    public void uiSendSms(View v){
        EditText phone = (EditText) findViewById(R.id.editText);
        String id = String.valueOf(((int) (Math.random() * 100)));
        sendSMS(phone.getText().toString(), "test", id);
    }

    public void scanQrCode(View v){
        new IntentIntegrator(this).setCaptureActivity(CustomCaptureActivity.class).initiateScan();
    }

    /*********************
    *   Intents Results  *
    **********************/

    private byte[] parseByte(String s){
        byte b[] = new byte[s.length()];
        for(int i =0;i<s.length();i++){
            b[i] = (byte) s.charAt(i);
        }
        return b;
    }
/*
    private String encrypt(String data){

        String encrypted = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key,iv
            );
            encrypted = Base64.encodeToString(cipher.doFinal(data.getBytes()), Base64.DEFAULT);
            //encrypted = encrypted.substring(0, encrypted.length() - 1);

        } catch (Exception e) {
            Toast.makeText(this, "Encrypt : "+e.getMessage(), Toast.LENGTH_LONG).show();
            Log.d("Encrypt",e.getMessage());
        }

        return encrypted;
    }
    */

    public void setToken(String t) {
        String[] token = t.split("#");
        this.passphrase = token[0];
        this.iv = token[1];
        this.salt = token[2];
        this.webid = token[3];
    }

    public void sendSocket(String chan, JSONObject data){
        socket.emit(chan,data);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result!=null) {
            Toast.makeText(this,"QR : " + result.getContents(), Toast.LENGTH_SHORT).show();
            String qr[] = result.getContents().split("#");
            Toast.makeText(this, "keylength : "+qr[0].getBytes().length, Toast.LENGTH_SHORT).show();

            /*key = new SecretKeySpec(parseByte(qr[0]), "AES");
            iv = new IvParameterSpec(parseByte(qr[1]));
            webid = qr[2];
            */
            setToken(result.getContents());
            socket.connect();
            JSONObject datas = new JSONObject();
            try {
                datas.put("id", webid);
                //datas.put("data","fef");

                datas.put("data", new AesUtil(256, 10).encrypt(salt, iv, passphrase, new JSONObject().put("type", "connection").toString()));
            } catch (JSONException e) {

                Toast.makeText(this, "SEND : failed", Toast.LENGTH_SHORT).show();
            }
            sendSocket("connection-phone", datas);

            /*** Send test ***/

            datas = new JSONObject();
            try {
                datas.put("id", webid);

                datas.put("data", new AesUtil(256, 10).encrypt(salt, iv, passphrase, new JSONObject().put("type", "msg").put("msg","Hello").toString()));
            } catch (JSONException e) {

                Toast.makeText(this, "SEND : failed", Toast.LENGTH_SHORT).show();
            }
            sendSocket("data", datas);
        }
        else{
            Toast.makeText(this, "QR READ : failed", Toast.LENGTH_SHORT).show();
        }

}
}
